/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.example.techcampteam01;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
    }
    public static final class drawable {
        public static final int apple=0x7f020000;
        public static final int back=0x7f020001;
        public static final int back1=0x7f020002;
        public static final int bg=0x7f020003;
        public static final int btnabout=0x7f020004;
        public static final int btnexit=0x7f020005;
        public static final int btnoption=0x7f020006;
        public static final int btnstart=0x7f020007;
        public static final int buttonback=0x7f020008;
        public static final int buttonclose=0x7f020009;
        public static final int buttonexit=0x7f02000a;
        public static final int buttonfire=0x7f02000b;
        public static final int buttonoption=0x7f02000c;
        public static final int buttonresume=0x7f02000d;
        public static final int buttonretry=0x7f02000e;
        public static final int buttonshare=0x7f02000f;
        public static final int buttonstart=0x7f020010;
        public static final int close=0x7f020011;
        public static final int close1=0x7f020012;
        public static final int egg=0x7f020013;
        public static final int exit=0x7f020014;
        public static final int exit1=0x7f020015;
        public static final int fire=0x7f020016;
        public static final int fire1=0x7f020017;
        public static final int guid=0x7f020018;
        public static final int highscore=0x7f020019;
        public static final int ic_launch9er=0x7f02001a;
        public static final int ic_launche1r=0x7f02001b;
        public static final int ic_launcher=0x7f02001c;
        public static final int ic_launchfer=0x7f02001d;
        public static final int logo=0x7f02001e;
        public static final int main=0x7f02001f;
        public static final int main1=0x7f020020;
        public static final int medal=0x7f020021;
        public static final int mute=0x7f020022;
        public static final int option=0x7f020023;
        public static final int option1=0x7f020024;
        public static final int optionbg=0x7f020025;
        public static final int pause=0x7f020026;
        public static final int pause_title=0x7f020027;
        public static final int plus1=0x7f020028;
        public static final int plus3=0x7f020029;
        public static final int plus5=0x7f02002a;
        public static final int resultbg=0x7f02002b;
        public static final int resume=0x7f02002c;
        public static final int resume1=0x7f02002d;
        public static final int retry=0x7f02002e;
        public static final int retry1=0x7f02002f;
        public static final int share=0x7f020030;
        public static final int share1=0x7f020031;
        public static final int splash=0x7f020032;
        public static final int splash1=0x7f020033;
        public static final int splash2=0x7f020034;
        public static final int start=0x7f020035;
        public static final int start1=0x7f020036;
        public static final int target=0x7f020037;
        public static final int timeup=0x7f020038;
        public static final int tomato=0x7f020039;
        public static final int tomato2=0x7f02003a;
        public static final int unmute=0x7f02003b;
    }
    public static final class id {
        public static final int action_settings=0x7f090017;
        public static final int btnBack=0x7f09000e;
        public static final int btnExit=0x7f090002;
        public static final int btnOption=0x7f090001;
        public static final int btnStart=0x7f090000;
        public static final int btn_mainmenu=0x7f09000b;
        public static final int btn_retry=0x7f09000a;
        public static final int btn_share=0x7f090009;
        public static final int btn_start=0x7f090016;
        public static final int checkbox_music=0x7f09000c;
        public static final int checkbox_sound=0x7f09000d;
        public static final int high_score=0x7f090008;
        public static final int img_view=0x7f090006;
        public static final int pause_btn=0x7f090011;
        public static final int preview=0x7f09000f;
        public static final int resume_button=0x7f090003;
        public static final int retry_button=0x7f090004;
        public static final int return_to_main=0x7f090005;
        public static final int text_view_highscore=0x7f090014;
        public static final int text_view_timer=0x7f090012;
        public static final int textview_score=0x7f090013;
        public static final int timeup_img=0x7f090015;
        public static final int tomato_fire=0x7f090010;
        public static final int tv_score=0x7f090007;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int pause_dialog=0x7f030001;
        public static final int result_layout=0x7f030002;
        public static final int setting=0x7f030003;
        public static final int single_play=0x7f030004;
    }
    public static final class menu {
        public static final int main=0x7f080000;
    }
    public static final class raw {
        public static final int highscore_claps_hand=0x7f040000;
        public static final int main_menu_sound=0x7f040001;
        public static final int sound_hit=0x7f040002;
        public static final int timer_out=0x7f040003;
        public static final int timer_stop=0x7f040004;
    }
    public static final class string {
        public static final int action_settings=0x7f060001;
        public static final int app_id=0x7f060006;
        public static final int app_name=0x7f060000;
        public static final int default_name=0x7f060005;
        public static final int hello_world=0x7f060002;
        public static final int highscore=0x7f060009;
        public static final int mainmenu=0x7f06000c;
        public static final int resume=0x7f06000d;
        public static final int retry=0x7f06000b;
        public static final int score=0x7f060007;
        public static final int share=0x7f060003;
        public static final int start=0x7f06000a;
        public static final int time=0x7f060008;
        public static final int your_name=0x7f060004;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
    }
}
